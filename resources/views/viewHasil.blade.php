<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>

<div class="container">

    <h1>Hasil</h1>
    <br>
    <table class=" table table-bordered table-stripped">
        <thead>
        <tr>
            <th scope="col">Angka</th>
            <th scope="col">Bilangan</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($data as $item => $value)
            <tr>
                <td>Angka {{ $item }} adalah</td>
                <td>{{ $value }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>
