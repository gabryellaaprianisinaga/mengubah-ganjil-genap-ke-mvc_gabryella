<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="form">
            <div class="p-3">
                <h2>=========== Cetak Ganjil Dan Genap ===========</h2>
            </div>
            <form action="/output" method="post">
                @csrf
                <label for="">Angka Pertama</label>
                <br>
                <input type="number"  name="angka1" />
                <br>
                <label for="">Angka Kedua</label>
                <br>
                <input type="number" name="angka2" />
                <br>
                <input type="submit" class="btn btn-primary mt-3" value="Output" />
            </form>
        </div>
    </div>
</body>
</html>
