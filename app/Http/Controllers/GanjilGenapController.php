<?php

namespace App\Http\Controllers;

use App\Models\GanjilGenap;
use Illuminate\Http\Request;

class GanjilGenapController extends Controller
{
    // method gangen
    public function gangen () {
        return view('ganjil_genap');
    }

    public function save(Request $request)
    {
        $arr = $request->all();
        GanjilGenap::setgangen($arr['angka1'], $arr['angka2']);
        GanjilGenap::cekgangen();
        return view('viewHasil',["data" => GanjilGenap::getOutput()]);
    }

}
