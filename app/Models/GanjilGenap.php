<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GanjilGenap extends Model
{
    use HasFactory;
    private static $angka1, $angka2;
    private static $arr = [];

    public static function cekgangen()
    {
        if(self::$angka1 < self::$angka2)
        {
            for($i = self::$angka1; $i <= self::$angka2; $i++)
            {
                ($i % 2 == 0) ? self::$arr["$i"] = "genap" : self::$arr["$i"] = "ganjil";
            }
        } else
        {
            for($i = self::$angka1; $i>=self::$angka2; $i--)
            {
                ($i % 2 == 0) ? self::$arr["$i"] = "genap" : self::$arr["$i"] = "ganjil";
            }
        }
    }

    public static function getOutput()
    {
        return self::$arr;
    }

    public static function setgangen($angka1, $angka2)
    {
        self::$angka1 = $angka1;
        self::$angka2 = $angka2;
    }
}
