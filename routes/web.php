<?php

use App\Http\Controllers\GanjilGenapController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

// Route untuk menentukan ganjil dan genap
Route::get("/", [GanjilGenapController::class, 'gangen']);
Route::post("/output", [GanjilGenapController::class, 'save']);
